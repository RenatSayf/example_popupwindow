package com.example_popupwindow;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener
{
    final String RESTORE_KEY = "restore_key";
    LinearLayout popupTop;
    LinearLayout popupBottom;
    TextView tvHelloWorld;
    FrameLayout mainLayout;
    PopupWindow popupWindow1;
    Var var;

    private class Var implements Parcelable
    {
        int windowWidth, windowHeight;
        double touchX, touchY;
        int popupTopVisibility = View.GONE;
        int popupBottomVisibility = View.GONE;
        boolean[] isShwoPopupView1;
        String selectedText;

        Var()
        {
            isShwoPopupView1 = new boolean[1];
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i)
        {
            parcel.writeInt(windowHeight);
            parcel.writeInt(windowWidth);
            parcel.writeDouble(touchX);
            parcel.writeDouble(touchY);
            parcel.writeInt(popupTopVisibility);
            parcel.writeInt(popupBottomVisibility);
            parcel.writeBooleanArray(isShwoPopupView1);
            parcel.writeString(selectedText);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null)
        {
            var = new Var();
        }
        else
        {
            var = savedInstanceState.getParcelable(RESTORE_KEY);
        }

        popupTop = (LinearLayout) findViewById(R.id.include_top);
        popupBottom = (LinearLayout) findViewById(R.id.include_bottom);

        popupTop.setVisibility(var.popupTopVisibility);
        popupBottom.setVisibility(var.popupBottomVisibility);

        Button btnTopPopup1 = (Button) popupTop.findViewById(R.id.btn_popup_1);
        btnTopPopup1.setOnClickListener(this);

        Button btnBottomPopup1 = (Button) popupBottom.findViewById(R.id.btn_popup_1);
        btnBottomPopup1.setOnClickListener(this);

        mainLayout = (FrameLayout) findViewById(R.id.layout_main);
        mainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                mainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                var.windowWidth = mainLayout.getWidth();
                var.windowHeight = mainLayout.getHeight();
            }
        });

        tvHelloWorld = (TextView) findViewById(R.id.tv_hello_world);
        tvHelloWorld.setOnTouchListener(this);
        tvHelloWorld.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                if (var.touchY <= var.windowHeight / 2.0)
                {
                    popupTop.setVisibility(View.GONE);
                    popupBottom.setVisibility(View.VISIBLE);
                    var.popupTopVisibility = popupTop.getVisibility();
                    var.popupBottomVisibility = popupBottom.getVisibility();
                }
                else
                {
                    popupBottom.setVisibility(View.GONE);
                    popupTop.setVisibility(View.VISIBLE);
                    var.popupTopVisibility = popupTop.getVisibility();
                    var.popupBottomVisibility = popupBottom.getVisibility();
                }
                return false;
            }
        });

        if (savedInstanceState != null)
        {
            mainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
            {
                @Override
                public void onGlobalLayout()
                {
                    mainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    if (var.isShwoPopupView1[0])
                    {
                        View popupView1 = getLayoutInflater().inflate(R.layout.popup_layout_1, null);
                        popupWindow1 = new PopupWindow(popupView1, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        popupWindow1.showAtLocation(mainLayout, Gravity.CENTER, 0, 0);
                        Button btnPopup1 = (Button) popupView1.findViewById(R.id.btn_popup_1);
                        btnPopup1.setOnClickListener(MainActivity.this);
                        TextView tvSelectedText = (TextView) popupView1.findViewById(R.id.tv_selected_text);
                        tvSelectedText.setText(var.selectedText);
                    }
                }
            });

        }
    }


    @Override
    public void onClick(View view)
    {
        if (popupWindow1 != null && popupWindow1.isShowing())
        {
            popupWindow1.dismiss();
            return;
        }
        var.selectedText = tvHelloWorld.getText().subSequence(tvHelloWorld.getSelectionStart(), tvHelloWorld.getSelectionEnd()).toString();
        LinearLayout layout = (LinearLayout) view.getParent();
        layout.setVisibility(View.GONE);
        var.popupTopVisibility = View.GONE;
        var.popupBottomVisibility = View.GONE;

        tvHelloWorld.clearFocus();
        View popupView1 = getLayoutInflater().inflate(R.layout.popup_layout_1, null);
        popupWindow1 = new PopupWindow(popupView1, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow1.showAtLocation(mainLayout, Gravity.CENTER, 0, 0);
        Button btnPopup1 = (Button) popupView1.findViewById(R.id.btn_popup_1);
        btnPopup1.setOnClickListener(this);
        TextView tvSelectedText = (TextView) popupView1.findViewById(R.id.tv_selected_text);
        tvSelectedText.setText(var.selectedText);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent)
    {
        var.touchX = motionEvent.getX();
        var.touchY = motionEvent.getY();
        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        if (popupWindow1 != null)
        {
            var.isShwoPopupView1[0] = popupWindow1.isShowing();
        }
        outState.putParcelable(RESTORE_KEY, var);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (popupWindow1 != null)
        {
            popupWindow1.dismiss();
        }
    }
}
